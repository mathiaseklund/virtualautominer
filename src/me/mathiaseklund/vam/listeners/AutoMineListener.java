package me.mathiaseklund.vam.listeners;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import me.mathiaseklund.vam.Main;
import me.mathiaseklund.vam.api.AutoMine;
import me.mathiaseklund.vam.util.Util;

public class AutoMineListener implements Listener {

	Main main = Main.getMain();

	ArrayList<String> checked = new ArrayList<String>();

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		ItemStack is = player.getItemInHand();

		Block b = event.getClickedBlock();
		if (b != null) {
			if (b.getType().toString().contains("BUTTON")) {
				Util.debug("Clicked on Button with Pickaxe");
				if (b.hasMetadata("automine")) {
					event.setCancelled(true);
					if (is != null) {
						if (is.getType().toString().contains("PICKAXE")) {
							AutoMine.clickedButton(player, is);
						} else {
							for (String s : main.getMessages().getStringList("automine.reqpick")) {
								Util.message(player, s);
							}
						}
					} else {
						for (String s : main.getMessages().getStringList("automine.reqpick")) {
							Util.message(player, s);
						}
					}
				}
			}
		}
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		if (AutoMine.isActivated(player.getUniqueId().toString())) {
			if (!player.hasPermission("automine.offline") || player.isOp()) {
				AutoMine.deactivateAutoMiner(player.getUniqueId().toString());
			}
		}
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		if (AutoMine.isActivated(player.getUniqueId().toString())) {
			if (!checked.contains(player.getName())) {
				checked.add(player.getName());

				String region = main.getConfig().getString("automine.region");
				Location loc = player.getLocation();
				ApplicableRegionSet set = WGBukkit.getRegionManager(player.getWorld()).getApplicableRegions(loc);
				boolean regionFound = false;
				for (ProtectedRegion r : set.getRegions()) {
					Util.debug("Found Region: " + r.getId());
					if (r.getId().equalsIgnoreCase(region)) {
						regionFound = true;
					}
				}
				if (!regionFound) {
					AutoMine.deactivateAutoMiner(player.getUniqueId().toString());
				}

				Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
					public void run() {
						checked.remove(player.getName());
					}
				}, 20);
			}
		}
	}
}
