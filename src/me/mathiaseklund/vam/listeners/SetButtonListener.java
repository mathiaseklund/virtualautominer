package me.mathiaseklund.vam.listeners;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import me.mathiaseklund.vam.Main;
import me.mathiaseklund.vam.api.AutoMine;
import me.mathiaseklund.vam.util.Util;

public class SetButtonListener implements Listener {

	Main main = Main.getMain();

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if (AutoMine.isSettingButton(player)) {
			event.setCancelled(true);
			Block b = event.getClickedBlock();
			if (b != null) {
				if (b.getType().toString().contains("BUTTON")) {
					Location loc = b.getLocation();
					AutoMine.setButton(player, loc);
				} else {
					Util.message(player, "&4ERROR:&7 Must click on a button.");
				}
			}
		}
	}

}
