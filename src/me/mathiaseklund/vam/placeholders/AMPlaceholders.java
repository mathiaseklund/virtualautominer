package me.mathiaseklund.vam.placeholders;

import org.bukkit.entity.Player;

import me.clip.placeholderapi.external.EZPlaceholderHook;
import me.mathiaseklund.vam.Main;
import me.mathiaseklund.vam.api.AutoMine;

@SuppressWarnings("deprecation")
public class AMPlaceholders extends EZPlaceholderHook {

	Main main = Main.getMain();

	public AMPlaceholders() {
		super(Main.getMain(), "automine");
	}

	@Override
	public String onPlaceholderRequest(Player player, String identifier) {
		// Placeholder: %exampleplugin_staff_count%
		if (identifier.equals("tier")) {
			// Since we return a String, we have to convert it from the integer
			return AutoMine.getHighestTier(player.getUniqueId().toString());
		} else if (identifier.equals("currentier")) {
			return AutoMine.getActiveTier(player.getUniqueId().toString());
		} else if (identifier.equals("time")) {
			return AutoMine.getTimeRemaining(player.getUniqueId().toString());
		}

		// We return null if any other identifier was provided
		return null;
	}
}
