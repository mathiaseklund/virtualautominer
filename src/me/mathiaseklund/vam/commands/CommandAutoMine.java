package me.mathiaseklund.vam.commands;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.vam.Main;
import me.mathiaseklund.vam.api.AutoMine;
import me.mathiaseklund.vam.util.Util;

public class CommandAutoMine implements CommandExecutor {

	Main main = Main.getMain();

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (args.length == 0) {
				// Teleport to autominer spot
				String location = main.getConfig().getString("automine.tp");
				if (location != null) {
					player.teleport(Util.StringToLocation(location));
					for (String s : main.getMessages().getStringList("automine.teleported")) {
						Util.message(player, s);
					}
				} else {
					for (String s : main.getMessages().getStringList("teleport.notset")) {
						Util.message(player, s);
					}
				}

			} else if (args.length == 1) {
				if (args[0].equalsIgnoreCase("settp")) {
					if (player.hasPermission("automine.admin") || player.isOp()) {
						AutoMine.settp(player);
					}
				} else if (args[0].equalsIgnoreCase("setbutton")) {
					if (player.hasPermission("automine.admin") || player.isOp()) {
						AutoMine.setButton(player);
					}
				} else if (args[0].equalsIgnoreCase("start")) {
					if (AutoMine.isActivated(player.getUniqueId().toString())) {
						for (String s : main.getMessages().getStringList("automine.alreadyactivated")) {
							Util.message(player, s);
						}
					} else {

						AutoMine.activateAutoMiner(player);

					}
				} else if (args[0].equalsIgnoreCase("stop")) {
					if (AutoMine.isActivated(player.getUniqueId().toString())) {
						AutoMine.deactivateAutoMiner(player.getUniqueId().toString());
					} else {
						for (String s : main.getMessages().getStringList("automine.notactivated")) {
							Util.message(player, s);
						}
					}
				} else if (args[0].equalsIgnoreCase("check")) {

					if (player.hasPermission("automine.check") || player.isOp()) {
						if (AutoMine.isActivated(player.getUniqueId().toString())) {
							for (String s : main.getMessages().getStringList("check.self")) {
								Util.message(player, s
										.replaceAll("%time%",
												AutoMine.getTimeRemaining(player.getUniqueId().toString()))
										.replaceAll("%tier%", AutoMine.getActiveTier(player.getUniqueId().toString())));
							}
						} else {
							for (String s : main.getMessages().getStringList("automine.notactivated")) {
								Util.message(player, s);
							}
						}
					}
				} else if (args[0].equalsIgnoreCase("reload")) {
					if (sender.hasPermission("automine.reload") || sender.isOp()) {
						main.reload();
						Util.message(sender, "&aReloaded VAM Data");
					}
				}
			} else if (args.length == 2) {
				if (args[0].equalsIgnoreCase("check")) {
					if (player.hasPermission("automine.check.others") || player.isOp()) {
						String t = args[1];
						OfflinePlayer target = Bukkit.getOfflinePlayer(t);
						if (target != null) {
							if (AutoMine.isActivated(target.getUniqueId().toString())) {
								Util.debug("Is activated");
								for (String s : main.getMessages().getStringList("check.other")) {
									Util.message(player,
											s.replaceAll("%time%",
													AutoMine.getTimeRemaining(target.getUniqueId().toString()))
													.replaceAll("%tier%",
															AutoMine.getActiveTier(target.getUniqueId().toString()))
													.replaceAll("%target%", target.getName()));
								}
							} else {
								for (String s : main.getMessages().getStringList("automine.notactivated")) {
									Util.message(player, s);
								}
							}
						} else {
							for (String s : main.getMessages().getStringList("targetnotfound")) {
								Util.message(player, s);
							}
						}
					}
				}
			} else if (args.length == 4) {
				if (args[0].equalsIgnoreCase("give")) { // /am give name tier time
					if (sender.hasPermission("automine.admin.give") || sender.isOp()) {
						String t = args[1];
						String tier = args[2];
						if (AutoMine.tierExists(tier)) {
							OfflinePlayer target = Bukkit.getOfflinePlayer(t);
							if (target != null) {
								if (Util.isInteger(args[3])) {
									int time = Integer.parseInt(args[3]);
									AutoMine.giveTime(sender, target, tier, time);
								} else {
									int time = 0;
									if (args[3].equalsIgnoreCase("permanent")) {
										AutoMine.giveTime(sender, target, tier, -1);
									} else if (args[3].contains("h")) {
										if (Util.isInteger(args[3].split("h")[0])) {
											time = Integer.parseInt(args[3].split("h")[0]) * 60 * 60;
											AutoMine.giveTime(sender, target, tier, time);
										}
									} else if (args[3].contains("m")) {
										if (Util.isInteger(args[3].split("m")[0])) {
											time = Integer.parseInt(args[3].split("m")[0]) * 60;
											AutoMine.giveTime(sender, target, tier, time);
										}
									} else if (args[3].contains("s")) {
										if (Util.isInteger(args[3].split("s")[0])) {
											time = Integer.parseInt(args[3].split("s")[0]);
											AutoMine.giveTime(sender, target, tier, time);
										}
									} else {
										for (String s : main.getMessages().getStringList("automine.invalidtime")) {
											Util.message(player, s);
										}
									}

								}
							} else {
								for (String s : main.getMessages().getStringList("targetnotfound")) {
									Util.message(player, s);
								}
							}
						} else {
							for (String s : main.getMessages().getStringList("tiernotfound")) {
								Util.message(player, s);
							}
						}
					}
				} else if (args[0].equalsIgnoreCase("remove")) { // /am remove name tier time
					if (sender.hasPermission("automine.admin.remove") || sender.isOp()) {
						String t = args[1];
						String tier = args[2];
						if (AutoMine.tierExists(tier)) {
							OfflinePlayer target = Bukkit.getOfflinePlayer(t);
							if (target != null) {
								if (Util.isInteger(args[3])) {
									int time = Integer.parseInt(args[3]);
									AutoMine.removeTime(sender, target, tier, time);
								} else {
									int time = 0;
									if (args[3].equalsIgnoreCase("all")) {
										time = 0;
										AutoMine.removeTime(sender, target, tier, time);
									} else if (args[3].contains("h")) {
										if (Util.isInteger(args[3].split("h")[0])) {
											time = Integer.parseInt(args[3].split("h")[0]) * 60 * 60;
											AutoMine.removeTime(sender, target, tier, time);
										}
									} else if (args[3].contains("m")) {
										if (Util.isInteger(args[3].split("m")[0])) {
											time = Integer.parseInt(args[3].split("m")[0]) * 60;
											AutoMine.removeTime(sender, target, tier, time);
										}
									} else if (args[3].contains("s")) {
										if (Util.isInteger(args[3].split("s")[0])) {
											time = Integer.parseInt(args[3].split("s")[0]);
											AutoMine.removeTime(sender, target, tier, time);
										}
									} else {
										for (String s : main.getMessages().getStringList("automine.invalidtime")) {
											Util.message(player, s);
										}
									}

								}
							} else {
								for (String s : main.getMessages().getStringList("targetnotfound")) {
									Util.message(player, s);
								}
							}
						} else {
							for (String s : main.getMessages().getStringList("tiernotfound")) {
								Util.message(player, s);
							}
						}
					}
				}
			}
		} else {
			// Console commands
			if (args.length == 4) {
				if (args[0].equalsIgnoreCase("give")) { // /am give name tier time
					if (sender.hasPermission("automine.admin.give") || sender.isOp()) {
						String t = args[1];
						String tier = args[2];
						if (AutoMine.tierExists(tier)) {
							OfflinePlayer target = Bukkit.getOfflinePlayer(t);
							if (target != null) {
								if (Util.isInteger(args[3])) {
									int time = Integer.parseInt(args[3]);
									AutoMine.giveTime(sender, target, tier, time);
								} else {
									int time = 0;
									if (args[3].equalsIgnoreCase("permanent")) {
										time = 100000 * 60 * 60;
										AutoMine.giveTime(sender, target, tier, time);
									} else if (args[3].contains("h")) {
										if (Util.isInteger(args[3].split("h")[0])) {
											time = Integer.parseInt(args[3].split("h")[0]) * 60 * 60;
											AutoMine.giveTime(sender, target, tier, time);
										}
									} else if (args[3].contains("m")) {
										if (Util.isInteger(args[3].split("m")[0])) {
											time = Integer.parseInt(args[3].split("m")[0]) * 60;
											AutoMine.giveTime(sender, target, tier, time);
										}
									} else if (args[3].contains("s")) {
										if (Util.isInteger(args[3].split("s")[0])) {
											time = Integer.parseInt(args[3].split("s")[0]);
											AutoMine.giveTime(sender, target, tier, time);
										}
									} else {
										for (String s : main.getMessages().getStringList("automine.invalidtime")) {
											Util.message(sender, s);
										}
									}
								}
							} else {
								for (String s : main.getMessages().getStringList("targetnotfound")) {
									Util.message(sender, s);
								}
							}
						} else {
							for (String s : main.getMessages().getStringList("tiernotfound")) {
								Util.message(sender, s);
							}
						}
					}
				} else if (args[0].equalsIgnoreCase("remove")) { // /am remove name tier time
					if (sender.hasPermission("automine.admin.remove") || sender.isOp()) {
						String t = args[1];
						String tier = args[2];
						if (AutoMine.tierExists(tier)) {
							OfflinePlayer target = Bukkit.getOfflinePlayer(t);
							if (target != null) {
								if (Util.isInteger(args[3])) {
									int time = Integer.parseInt(args[3]);
									AutoMine.removeTime(sender, target, tier, time);
								} else {
									int time = 0;
									if (args[3].equalsIgnoreCase("all")) {
										time = 0;
										AutoMine.removeTime(sender, target, tier, time);
									} else if (args[3].contains("h")) {
										if (Util.isInteger(args[3].split("h")[0])) {
											time = Integer.parseInt(args[3].split("h")[0]) * 60 * 60;
											AutoMine.removeTime(sender, target, tier, time);
										}
									} else if (args[3].contains("m")) {
										if (Util.isInteger(args[3].split("m")[0])) {
											time = Integer.parseInt(args[3].split("m")[0]) * 60;
											AutoMine.removeTime(sender, target, tier, time);
										}
									} else if (args[3].contains("s")) {
										if (Util.isInteger(args[3].split("s")[0])) {
											time = Integer.parseInt(args[3].split("s")[0]);
											AutoMine.removeTime(sender, target, tier, time);
										}
									} else {
										for (String s : main.getMessages().getStringList("automine.invalidtime")) {
											Util.message(sender, s);
										}
									}

								}
							} else {
								for (String s : main.getMessages().getStringList("targetnotfound")) {
									Util.message(sender, s);
								}
							}
						} else {
							for (String s : main.getMessages().getStringList("tiernotfound")) {
								Util.message(sender, s);
							}
						}
					}
				}
			}
		}
		return false;
	}

}
