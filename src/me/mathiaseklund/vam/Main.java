package me.mathiaseklund.vam;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

import me.mathiaseklund.vam.api.AutoMine;
import me.mathiaseklund.vam.commands.CommandAutoMine;
import me.mathiaseklund.vam.listeners.AutoMineListener;
import me.mathiaseklund.vam.listeners.BlockListener;
import me.mathiaseklund.vam.listeners.SetButtonListener;
import me.mathiaseklund.vam.placeholders.AMPlaceholders;
import net.lightshard.prisonmines.PrisonMines;
import net.milkbowl.vault.permission.Permission;

public class Main extends JavaPlugin {

	static Main main;
	public static Permission perms;
	public static PrisonMines pm;
	public static WorldGuardPlugin wg;

	// Files
	File messagesFile;
	FileConfiguration messages;
	File dataFile;
	FileConfiguration data;

	/**
	 * Get instance of Main class
	 * 
	 * @return Main class
	 */
	public static Main getMain() {
		return main;
	}

	/**
	 * Plugin is enabled
	 */
	public void onEnable() {
		main = this;

		LoadYMLFiles();
		RegisterListeners();
		RegisterCommands();
		setupPermissions();
		wg = getWorldGuard();
		pm = PrisonMines.getInstance();
		AutoMine.loadButton();

		if (Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI")) {
			registerPlaceholders();
		}
	}

	/**
	 * Register placeholders
	 */
	@SuppressWarnings("deprecation")
	public void registerPlaceholders() {
		new AMPlaceholders().hook();
	}

	/**
	 * Loa default .yml files
	 */
	void LoadYMLFiles() {
		this.saveDefaultConfig();

		/**
		 * Messages file
		 */
		messagesFile = new File(getDataFolder(), "messages.yml");
		if (!messagesFile.exists()) {
			messagesFile.getParentFile().mkdirs();
			saveResource("messages.yml", false);
		}

		messages = new YamlConfiguration();
		try {
			messages.load(messagesFile);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
		/**
		 * Data file
		 */
		dataFile = new File(getDataFolder(), "data.yml");
		if (!dataFile.exists()) {
			dataFile.getParentFile().mkdirs();
			saveResource("data.yml", false);
		}

		data = new YamlConfiguration();
		try {
			data.load(dataFile);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Save messages file
	 */
	public void saveMessages() {
		try {
			messages.save(messagesFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Get Messages config
	 * 
	 * @return
	 */
	public FileConfiguration getMessages() {
		return this.messages;
	}

	/**
	 * Save data file
	 */
	public void saveData() {
		try {
			data.save(dataFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Get Data config
	 * 
	 * @return
	 */
	public FileConfiguration getData() {
		return this.data;
	}

	/**
	 * Register Event Listener classes
	 */
	void RegisterListeners() {
		getServer().getPluginManager().registerEvents(new BlockListener(), this);
		getServer().getPluginManager().registerEvents(new SetButtonListener(), this);
		getServer().getPluginManager().registerEvents(new AutoMineListener(), this);
	}

	/**
	 * Register command executors
	 */
	void RegisterCommands() {
		getCommand("automine").setExecutor(new CommandAutoMine());
	}

	private boolean setupPermissions() {
		if (getServer().getPluginManager().getPlugin("Vault") == null) {
			return false;
		}
		RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
		perms = rsp.getProvider();
		return perms != null;
	}

	private WorldGuardPlugin getWorldGuard() {
		Plugin plugin = getServer().getPluginManager().getPlugin("WorldGuard");

		// WorldGuard may not be loaded
		if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
			return null; // Maybe you want throw an exception instead
		}

		return (WorldGuardPlugin) plugin;
	}

	/**
	 * Reload Config
	 */
	public void reload() {
		this.reloadConfig();

		/**
		 * Messages file
		 */
		messagesFile = new File(getDataFolder(), "messages.yml");
		if (!messagesFile.exists()) {
			messagesFile.getParentFile().mkdirs();
			saveResource("messages.yml", false);
		}

		messages = new YamlConfiguration();
		try {
			messages.load(messagesFile);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
		/**
		 * Data file
		 */
		dataFile = new File(getDataFolder(), "data.yml");
		if (!dataFile.exists()) {
			dataFile.getParentFile().mkdirs();
			saveResource("data.yml", false);
		}

		data = new YamlConfiguration();
		try {
			data.load(dataFile);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}

		AutoMine.loadButton();

		if (Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI")) {
			registerPlaceholders();
		}
	}
}
