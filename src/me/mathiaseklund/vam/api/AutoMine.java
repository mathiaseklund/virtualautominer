package me.mathiaseklund.vam.api;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;
import org.bukkit.metadata.FixedMetadataValue;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import io.netty.util.internal.ThreadLocalRandom;
import me.clip.autosell.AutoSell;
import me.mathiaseklund.vam.Main;
import me.mathiaseklund.vam.util.Util;
import net.lightshard.prisonmines.mine.Mine;

public class AutoMine {

	static ArrayList<String> settingbutton = new ArrayList<String>();
	static HashMap<String, Integer> settingbutton_task = new HashMap<String, Integer>();
	static HashMap<String, Integer> automining = new HashMap<String, Integer>();
	static HashMap<String, Boolean> receivedperm = new HashMap<String, Boolean>();
	static HashMap<String, String> activetier = new HashMap<String, String>();
	static HashMap<String, Long> deactivatetime = new HashMap<String, Long>();
	static HashMap<String, String> autominerworld = new HashMap<String, String>();

	/**
	 * Set the automine teleport location to players current location.
	 * 
	 * @param player
	 */
	public static void settp(Player player) {
		Main main = Main.getMain();
		main.getConfig().set("automine.tp", Util.LocationToString(player.getLocation()));
		main.saveConfig();
		for (String s : main.getMessages().getStringList("automine.settp")) {
			Util.message(player, s);
		}
	}

	/**
	 * Set the automine button location.
	 * 
	 * @param player
	 */
	public static void setButton(Player player) {
		Main main = Main.getMain();
		if (!settingbutton.contains(player.getName())) {
			settingbutton.add(player.getName());
			for (String s : main.getMessages().getStringList("automine.setbutton.click")) {
				Util.message(player, s);
			}
			int task = Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
				public void run() {
					Util.debug("Set Button request timed out.");
					if (settingbutton.contains(player.getName())) {
						settingbutton.remove(player.getName());
						Util.message(player, main.getMessages().getString("automine.setbutton.timed"));
					}
				}
			}, 20 * 20);
			settingbutton_task.put(player.getName(), task);
		} else {
			for (String s : main.getMessages().getStringList("automine.setbutton.setting")) {
				Util.message(player, s);
			}
		}
	}

	/**
	 * Check if player is in the process of setting button location
	 * 
	 * @param player
	 * @return true/false
	 */
	public static boolean isSettingButton(Player player) {
		if (settingbutton.contains(player.getName())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Set button location
	 * 
	 * @param player
	 * @param loc
	 */
	public static void setButton(Player player, Location loc) {
		Main main = Main.getMain();

		String location = Util.LocationToString(loc);
		loc.getBlock().setMetadata("automine", new FixedMetadataValue(main, true));

		List<String> locations = main.getConfig().getStringList("automine.buttons");
		if (!locations.contains(location)) {
			locations.add(location);
		}
		main.getConfig().set("automine.buttons", locations);
		main.saveConfig();

		for (String s : main.getMessages().getStringList("automine.setbutton.set")) {
			Util.message(player, s);
		}
		settingbutton.remove(player.getName());
		int task = settingbutton_task.get(player.getName());
		Bukkit.getScheduler().cancelTask(task);

	}

	/**
	 * Load button
	 */
	public static void loadButton() {
		Main main = Main.getMain();
		List<String> locations = main.getConfig().getStringList("automine.buttons");
		for (String location : locations) {
			if (location != null) {
				Location loc = Util.StringToLocation(location);
				Block b = loc.getBlock();
				if (b != null) {
					if (b.getType().toString().contains("BUTTON")) {
						b.setMetadata("automine", new FixedMetadataValue(main, true));
					} else {
						Util.debug("Block is not a button at location");
					}
				} else {
					Util.debug("Block not found at button location.");
				}
			} else {
				Util.debug("Could not found button location");
			}
		}
	}

	/**
	 * Get mine associated with group.
	 * 
	 * @param rank
	 * @return
	 */
	public static String getTierMine(String tier) {
		Main main = Main.getMain();
		String mine = main.getConfig().getString("automine.tiers." + tier + ".mine");
		if (mine != null) {
			return mine;
		} else {
			return null;
		}
	}

	/**
	 * Player clicked automine button
	 * 
	 * @param player
	 * @param pick
	 */
	public static void clickedButton(Player player, ItemStack pick) {
		Main main = Main.getMain();
		Util.debug(player.getName() + " clicked on automine button with a pickaxe");

		String tier = getHighestTier(player.getUniqueId().toString());
		if (tier != null) {
			int time = getTime(player.getUniqueId().toString(), tier);
			Util.debug("Found tier: " + tier);
			String mine = getTierMine(tier);
			if (mine != null) {
				Util.debug("Mine found: " + mine);
				Collection<Mine> mines = Main.pm.getMineManager().getMines();
				for (Mine m : mines) {
					Util.debug("Found mine: " + m.getName());
					if (m.getName().equalsIgnoreCase(mine)) {
						Util.debug("Found group mine.");
						if (isActivated(player.getUniqueId().toString())) {
							deactivateAutoMiner(player.getUniqueId().toString());
						} else {
							if (time > 0) {
								activateAutoMiner(player, m, tier, pick);
							} else {
								if (isPermanent(player.getUniqueId().toString(), tier)) {
									activateAutoMiner(player, m, tier, pick);
								} else {
									for (String s : main.getMessages().getStringList("automine.notime")) {
										Util.message(player, s);
									}
								}
							}

						}
						break;
					}
				}
			} else {
				Util.debug("No mine found for the tier: " + tier);
			}
		} else {
			for (String s : main.getMessages().getStringList("automine.notime")) {
				Util.message(player, s);
			}
		}
	}

	/**
	 * Get users AutoMine time remaining
	 * 
	 * @param uuid
	 * @return
	 */
	public static int getTime(String uuid, String tier) {
		Main main = Main.getMain();
		return main.getData().getInt(uuid + "." + tier + ".time");
	}

	/**
	 * Give automine time to player
	 * 
	 * @param sender
	 * @param target
	 * @param time
	 */
	public static void giveTime(CommandSender sender, OfflinePlayer target, String tier, int time) {
		Main main = Main.getMain();
		if (isActivated(target.getUniqueId().toString())) {
			deactivateAutoMiner(target.getUniqueId().toString());
		}
		Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
			public void run() {
				if (time == -1) {
					setPermanent(target.getUniqueId().toString(), tier, true);
					for (String s : main.getMessages().getStringList("give.time.sender")) {
						Util.message(sender, s.replaceAll("%time%", "permenant").replaceAll("%tier%", getTierName(tier))
								.replaceAll("%target%", target.getName()));
					}

					if (target.isOnline()) {
						for (String s : main.getMessages().getStringList("give.time.target")) {
							Util.message(target.getPlayer(),
									s.replaceAll("%time%", "permanent").replaceAll("%tier%", getTierName(tier)));
						}

					}
				} else {
					int ot = getTime(target.getUniqueId().toString(), tier);
					ot += time;
					setTime(target.getUniqueId().toString(), tier, ot);
					setLastGivenTime(target.getUniqueId().toString(), tier, ot);
					int[] ints = Util.splitSeconds(new BigDecimal(time));
					int seconds = ints[2];
					int minutes = ints[1];
					int hours = ints[0];
					String timeString = null;
					if (hours > 0) {
						timeString = hours + "h";
					}
					if (minutes > 0) {
						if (timeString == null) {
							timeString = minutes + "m";
						} else {
							timeString += " " + minutes + "m";
						}
					}
					if (seconds > 0) {
						if (timeString == null) {
							timeString = seconds + "s";
						} else {
							timeString += " " + seconds + "s";
						}
					}
					for (String s : main.getMessages().getStringList("give.time.sender")) {
						Util.message(sender, s.replaceAll("%time%", timeString).replaceAll("%tier%", getTierName(tier))
								.replaceAll("%target%", target.getName()));
					}

					if (target.isOnline()) {
						for (String s : main.getMessages().getStringList("give.time.target")) {
							Util.message(target.getPlayer(),
									s.replaceAll("%time%", timeString).replaceAll("%tier%", getTierName(tier)));
						}

					}
				}
			}
		}, 1);

	}

	/**
	 * Remove time
	 * 
	 * @param sender
	 * @param target
	 * @param tier
	 * @param time
	 */
	public static void removeTime(CommandSender sender, OfflinePlayer target, String tier, int time) {
		Main main = Main.getMain();
		if (isActivated(target.getUniqueId().toString())) {
			deactivateAutoMiner(target.getUniqueId().toString());
		}
		Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
			public void run() {
				if (time == -1) {
					setPermanent(target.getUniqueId().toString(), tier, false);
					for (String s : main.getMessages().getStringList("give.time.sender")) {
						Util.message(sender, s.replaceAll("%time%", "permenant").replaceAll("%tier%", getTierName(tier))
								.replaceAll("%target%", target.getName()));
					}

					if (target.isOnline()) {
						for (String s : main.getMessages().getStringList("give.time.target")) {
							Util.message(target.getPlayer(),
									s.replaceAll("%time%", "permanent").replaceAll("%tier%", getTierName(tier)));
						}

					}
				} else {
					int ot = getTime(target.getUniqueId().toString(), tier);
					ot -= time;
					setTime(target.getUniqueId().toString(), tier, ot);
					setLastGivenTime(target.getUniqueId().toString(), tier, ot);
					int[] ints = Util.splitSeconds(new BigDecimal(time));
					int seconds = ints[2];
					int minutes = ints[1];
					int hours = ints[0];
					String timeString = null;
					if (hours > 0) {
						timeString = hours + "h";
					}
					if (minutes > 0) {
						if (timeString == null) {
							timeString = minutes + "m";
						} else {
							timeString += " " + minutes + "m";
						}
					}
					if (seconds > 0) {
						if (timeString == null) {
							timeString = seconds + "s";
						} else {
							timeString += " " + seconds + "s";
						}
					}
					if (timeString == null) {
						timeString = "all time";
					}
					for (String s : main.getMessages().getStringList("remove.time.sender")) {
						Util.message(sender, s.replaceAll("%time%", timeString).replaceAll("%tier%", getTierName(tier))
								.replaceAll("%target%", target.getName()));
					}

					if (target.isOnline()) {
						for (String s : main.getMessages().getStringList("remove.time.target")) {
							Util.message(target.getPlayer(),
									s.replaceAll("%time%", timeString).replaceAll("%tier%", getTierName(tier)));
						}

					}
				}
			}
		}, 1);

	}

	/**
	 * Set the last given time.
	 * 
	 * @param uuid
	 * @param tier
	 * @param time
	 */
	public static void setLastGivenTime(String uuid, String tier, int time) {
		Main main = Main.getMain();
		long now = Calendar.getInstance().getTimeInMillis();
		main.getData().set(uuid + "." + tier + ".lastgiven", now);
		main.saveData();
	}

	/**
	 * set players remaining automine time
	 * 
	 * @param uuid
	 * @param time
	 */
	public static void setTime(String uuid, String tier, int time) {
		Main main = Main.getMain();
		if (time < 0) {
			time = 0;
		}
		main.getData().set(uuid + "." + tier + ".time", time);
		main.saveData();
	}

	/**
	 * Check if player has auto miner activated
	 * 
	 * @param player
	 * @return
	 */
	public static boolean isActivated(String uuid) {
		if (activetier.containsKey(uuid)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Deactivate Auto Miner
	 * 
	 * @param player
	 */
	public static void deactivateAutoMiner(String uuid) {
		Main main = Main.getMain();

		String tier = activetier.get(uuid);
		activetier.remove(uuid);

		long now = Calendar.getInstance().getTimeInMillis();
		long deactivate = main.getData().getLong(uuid + "." + tier + ".deactivate");
		int remaining = 0;
		if (deactivate > now) {
			remaining = (int) ((deactivate - now) / 1000);
		}
		if (remaining > 0) {
			main.getData().set(uuid + "." + tier + ".activated", null);
			main.getData().set(uuid + "." + tier + ".deactivate", null);
			main.getData().set(uuid + "." + tier + ".mine", null);
			main.getData().set(uuid + "." + tier + ".active", false);
			main.saveData();
			setTime(uuid, tier, remaining);
		} else {
			if (!isPermanent(uuid, tier)) {

				main.getData().set(uuid + "." + tier, null);
				main.saveData();
			} else {
				main.getData().set(uuid + "." + tier + ".activated", null);
				main.getData().set(uuid + "." + tier + ".deactivate", null);
				main.getData().set(uuid + "." + tier + ".mine", null);
				main.getData().set(uuid + "." + tier + ".active", false);
				main.saveData();
			}
		}

		deactivatetime.remove(uuid);
		stopAutoMiner(uuid);
		Util.debug("Deactivated Auto Miner");
		Player player = Bukkit.getPlayer(UUID.fromString(uuid));
		if (player != null) {
			for (String s : main.getMessages().getStringList("automine.deactivated")) {
				Util.message(player, s);
			}
		}
	}

	/**
	 * Activate auto miner
	 * 
	 * @param player
	 * @param mine
	 */
	public static void activateAutoMiner(Player player, Mine mine, String tier, ItemStack is) {
		Main main = Main.getMain();

		String region = main.getConfig().getString("automine.region");
		try {
			Sound sound = Sound.valueOf(main.getConfig().getString("automine.sound.button-click"));
			if (sound != null) {
				player.playSound(player.getLocation(), sound, 1, 1);
			} else {
				Util.debug("Sound Effect is INVALID");
			}
		} catch (Exception e) {
			Util.debug("Sound Effect is INVALID");
		}
		Location loc = player.getLocation();
		ApplicableRegionSet set = WGBukkit.getRegionManager(player.getWorld()).getApplicableRegions(loc);
		boolean regionFound = false;
		for (ProtectedRegion r : set.getRegions()) {
			Util.debug("Found Region: " + r.getId());
			if (r.getId().equalsIgnoreCase(region)) {
				regionFound = true;
			}
		}
		if (regionFound) {
			boolean canMine = true;
			String uuid = player.getUniqueId().toString();
			if (!player.hasPermission("automine.noreset")) {
				long lastGiven = main.getData().getLong(uuid + "." + tier + ".lastgiven");
				long timelimit = main.getConfig().getInt("automine.time-before-reset");
				if (lastGiven > 0) {
					long now = Calendar.getInstance().getTimeInMillis();
					long diff = now - lastGiven;
					diff = diff / 1000;
					Util.debug("Difference: " + diff);
					if (diff >= timelimit) {
						canMine = false;
						main.getData().set(uuid + "." + tier, null);
						main.saveData();
					}
				}
			}
			if (canMine) {
				activetier.put(uuid, tier);
				int time = getTime(uuid, tier);
				// setTime(uuid, tier, 0);

				long now = Calendar.getInstance().getTimeInMillis();
				long deactivate = now + (time * 1000);
				deactivatetime.put(uuid, deactivate);
				main.getData().set(uuid + "." + tier + ".activated", now);
				main.getData().set(uuid + "." + tier + ".deactivate", deactivate);
				main.getData().set(uuid + "." + tier + ".active", true);
				main.getData().set(uuid + "." + tier + ".mine", mine.getName());
				main.saveData();

				startAutoMiner(player, mine, is, deactivate);
			} else {
				for (String s : main.getMessages().getStringList("automine.notime")) {
					Util.message(player, s);
				}
			}
		} else {
			for (String s : main.getMessages().getStringList("automine.wrongregion")) {
				Util.message(player, s);
			}
		}
	}

	/**
	 * Activate auto miner
	 * 
	 * @param player
	 */
	public static void activateAutoMiner(Player player) { // TODO time check
		Main main = Main.getMain();
		String region = main.getConfig().getString("automine.region");

		Location loc = player.getLocation();
		ApplicableRegionSet set = WGBukkit.getRegionManager(player.getWorld()).getApplicableRegions(loc);
		boolean regionFound = false;
		for (ProtectedRegion r : set.getRegions()) {
			Util.debug("Found Region: " + r.getId());
			if (r.getId().equalsIgnoreCase(region)) {
				regionFound = true;
			}
		}
		if (regionFound) {
			String tier = getHighestTier(player.getUniqueId().toString());
			if (tier != null) {
				boolean canMine = true;
				String uuid = player.getUniqueId().toString();
				if (!player.hasPermission("automine.noreset")) {
					long lastGiven = main.getData().getLong(uuid + "." + tier + ".lastgiven");
					long timelimit = main.getConfig().getInt("automine.time-before-reset");
					if (lastGiven > 0) {
						long now = Calendar.getInstance().getTimeInMillis();
						long diff = now - lastGiven;
						diff = diff / 1000;
						Util.debug("Difference: " + diff);
						if (diff >= timelimit) {
							if (!isPermanent(uuid, tier)) {
								canMine = false;
								main.getData().set(uuid + "." + tier, null);
								main.saveData();
							}
						}
					}
				}
				if (canMine) {
					String m = getTierMine(tier);
					if (m != null) {
						Collection<Mine> mines = Main.pm.getMineManager().getMines();
						ItemStack is = player.getItemInHand();
						if (is != null) {
							if (is.getType().toString().contains("PICKAXE")) {
								for (Mine mine : mines) {
									if (mine.getName().equalsIgnoreCase(m)) {
										int time = getTime(uuid, tier);
										// setTime(uuid, tier, 0);

										long now = Calendar.getInstance().getTimeInMillis();
										long deactivate = now + (time * 1000);

										deactivatetime.put(uuid, deactivate);

										main.getData().set(uuid + "." + tier + ".activated", now);
										main.getData().set(uuid + "." + tier + ".deactivate", deactivate);
										main.getData().set(uuid + "." + tier + ".active", true);
										main.getData().set(uuid + "." + tier + ".mine", mine.getName());
										main.saveData();

										startAutoMiner(player, mine, is, deactivate);
										break;
									}
								}
							} else {
								for (String s : main.getMessages().getStringList("automine.reqpick")) {
									Util.message(player, s);
								}
							}
						} else {
							for (String s : main.getMessages().getStringList("automine.reqpick")) {
								Util.message(player, s);
							}
						}
					} else {
						for (String s : main.getMessages().getStringList("automine.nomine")) {
							Util.message(player, s);
						}
					}
				} else {
					for (String s : main.getMessages().getStringList("automine.notime")) {
						Util.message(player, s);
					}
				}

			} else {
				for (String s : main.getMessages().getStringList("automine.notime")) {
					Util.message(player, s);
				}
			}
		} else {
			for (String s : main.getMessages().getStringList("automine.wrongregion")) {
				Util.message(player, s);
			}
		}

	}

	/**
	 * Start auto mining for user
	 * 
	 * @param uuid
	 * @param mine
	 * @param is
	 */
	@SuppressWarnings("deprecation")
	public static void startAutoMiner(Player player, Mine mine, ItemStack is, long deactivate) {
		Util.debug("Starting auto miner");
		Main main = Main.getMain();
		for (String s : main.getMessages().getStringList("automine.activated")) {
			Util.message(player, s);
		}
		String asperm = main.getConfig().getString("automine.autosell.permission");
		if (!player.hasPermission(asperm)) {
			Main.perms.playerAdd(player, asperm);
			Main.perms.playerAdd(player, "autosell.shop.*");
			receivedperm.put(player.getUniqueId().toString(), false);
			autominerworld.put(player.getName(), player.getWorld().getName());
		}
		int task = Bukkit.getScheduler().scheduleAsyncRepeatingTask(main, new Runnable() {
			public void run() {
				ItemStack hand = player.getItemInHand();
				if (hand.getType().toString().contains("PICKAXE")) {
					long now = Calendar.getInstance().getTimeInMillis();
					if (now >= deactivate) {
						if (!isPermanent(player.getUniqueId().toString(),
								getActiveTier(player.getUniqueId().toString()))) {
							deactivateAutoMiner(player.getUniqueId().toString());
						}
					} else {
						// Util.debug("Mining a block");
						Map<MaterialData, Double> comp = mine.getBlocks();
						Set<MaterialData> keys = comp.keySet();
						int size = keys.size();
						int r = ThreadLocalRandom.current().nextInt(0, size);
						// Util.debug("Size: " + size);
						// Util.debug("Index: " + r);
						MaterialData sb = (MaterialData) keys.toArray()[r];
						while (mine.getRegion().iterator().hasNext()) {
							Bukkit.getScheduler().runTask(main, new Runnable() {
								public void run() {
									if (!AutoSell.inSellMode(player)) {
										// Util.debug("Adding to AutoSell");
										Bukkit.dispatchCommand(player, "autosell");
									}

									Block b = mine.getRegion().iterator().next();
									String bType = b.getType().toString();
									for (int i = 0; i < 10; i++) {
										// Util.debug("BlockID: " + sb.getItemTypeId());
										b.setTypeIdAndData(sb.getItemTypeId(), sb.getData(), false);
										// ((CraftPlayer) player).getHandle().playerInteractManager
										// .breakBlock(new BlockPosition(b.getX(), b.getY(), b.getZ()));

										// b.breakNaturally(is);
										Bukkit.getServer().getPluginManager().callEvent(new BlockBreakEvent(b, player));

										b.setType(Material.getMaterial(bType));
									}

								}
							});
							break;
						}

					}
				} else {
					deactivateAutoMiner(player.getUniqueId().toString());
				}
			}
		}, 20, 20);

		automining.put(player.getUniqueId().toString(), task);
	}

	/**
	 * Stop auto mining for player
	 * 
	 * @param uuid
	 */
	public static void stopAutoMiner(String uuid) {
		Main main = Main.getMain();
		if (automining.containsKey(uuid)) {
			if (receivedperm.containsKey(uuid)) {
				Player player = Bukkit.getPlayer(UUID.fromString(uuid));
				String asperm = main.getConfig().getString("automine.autosell.permission");
				if (player != null) {
					autominerworld.remove(player.getName());
					Main.perms.playerRemove(player, asperm);
					Main.perms.playerRemove(player, "autosell.shop.*");
					if (AutoSell.inSellMode(player)) {
						Bukkit.dispatchCommand(player, "autosell");
						AutoSell.inSellMode.remove(player.getName());
					}
				} else {
					OfflinePlayer op = Bukkit.getOfflinePlayer(UUID.fromString(uuid));
					String world = autominerworld.get(op.getName());
					autominerworld.remove(op.getName());
					if (op != null) {
						Main.perms.playerRemove(world, op, asperm);
						Main.perms.playerRemove(world, player, "autosell.shop.*");
						AutoSell.inSellMode.remove(op.getName());

					}
				}

			}
			int task = automining.get(uuid);
			automining.remove(uuid);
			Bukkit.getScheduler().cancelTask(task);
			Util.debug("Stopped auto mining.");
		}
	}

	/**
	 * Check if tier exists
	 * 
	 * @param tier
	 * @return
	 */
	public static boolean tierExists(String tier) {
		Set<String> tiers = getTiers();
		if (tiers.contains(tier)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Get all tiers
	 * 
	 * @return
	 */
	public static Set<String> getTiers() {
		Main main = Main.getMain();
		Set<String> tiers = main.getConfig().getConfigurationSection("automine.tiers").getKeys(false);
		return tiers;
	}

	/**
	 * Get all tiers for user
	 * 
	 * @param uuid
	 * @return
	 */
	public static Set<String> getTiers(String uuid) {
		Main main = Main.getMain();
		if (main.getData().getConfigurationSection(uuid) != null) {
			Set<String> tiers = main.getData().getConfigurationSection(uuid).getKeys(false);
			return tiers;
		} else {
			return null;
		}
	}

	/**
	 * Get highest tier for user
	 * 
	 * @param uuid
	 * @return
	 */
	public static String getHighestTier(String uuid) {
		Main main = Main.getMain();
		Set<String> tiers = getTiers(uuid);
		if (tiers != null) {
			String highest = null;
			int current = -1;
			for (String s : tiers) {
				if (!main.getData().getBoolean(uuid + "." + s + ".permanent")) {
					if (main.getData().getInt(uuid + "." + s + ".time") > 0) {
						int prio = main.getConfig().getInt("automine.tiers." + s + ".priority");
						if (prio > current) {
							highest = s;
							current = prio;
						}
					}
				} else {
					int prio = main.getConfig().getInt("automine.tiers." + s + ".priority");
					if (prio > current) {
						highest = s;
						current = prio;
					}
				}
			}
			return highest;
		} else {
			return null;
		}
	}

	/**
	 * Get name of tier
	 * 
	 * @param tier
	 * @return
	 */
	public static String getTierName(String tier) {
		Main main = Main.getMain();
		return main.getConfig().getString("automine.tiers." + tier + ".name");
	}

	/**
	 * Get currently active tier for user
	 * 
	 * @param uuid
	 * @return
	 */
	public static String getActiveTier(String uuid) {
		return activetier.get(uuid);
	}

	/**
	 * Get current time remaining for user.
	 * 
	 * @param uuid
	 * @return
	 */
	public static String getTimeRemaining(String uuid) {
		long deactivate = deactivatetime.get(uuid);
		long now = Calendar.getInstance().getTimeInMillis();

		if (deactivate >= now) {
			int time = (int) ((deactivate - now) / 1000);
			int[] ints = Util.splitSeconds(new BigDecimal(time));
			int seconds = ints[2];
			int minutes = ints[1];
			int hours = ints[0];
			String timeString = null;
			if (hours > 0) {
				timeString = hours + "h";
			}
			if (minutes > 0) {
				if (timeString == null) {
					timeString = minutes + "m";
				} else {
					timeString += " " + minutes + "m";
				}
			}
			if (seconds > 0) {
				if (timeString == null) {
					timeString = seconds + "s";
				} else {
					timeString += " " + seconds + "s";
				}
			}
			return timeString;
		} else {
			return "No Time";
		}
	}

	/**
	 * set permanent status
	 * 
	 * @param uuid
	 * @param tier
	 * @param b
	 */
	public static void setPermanent(String uuid, String tier, boolean b) {
		Main main = Main.getMain();
		main.getData().set(uuid + "." + tier + ".permanent", b);
		main.saveData();

	}

	/**
	 * Check if player has permanent time for tier
	 * 
	 * @param uuid
	 * @param tier
	 * @return
	 */
	public static boolean isPermanent(String uuid, String tier) {
		Main main = Main.getMain();
		return main.getData().getBoolean(uuid + "." + tier + ".permanent");
	}
}
